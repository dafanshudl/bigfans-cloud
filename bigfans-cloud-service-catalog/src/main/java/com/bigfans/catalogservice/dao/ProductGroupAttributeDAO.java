package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.ProductGroupAttribute;
import com.bigfans.framework.dao.BaseDAO;

import java.util.List;

/**
 * 
 * @Title: 
 * @Description: 商品和属性值关联表DAO
 * @author lichong 
 * @date 2015年12月21日 下午9:55:11 
 * @version V1.0
 */
public interface ProductGroupAttributeDAO extends BaseDAO<ProductGroupAttribute>{

	List<ProductGroupAttribute> listByPgId(String pgId);
}

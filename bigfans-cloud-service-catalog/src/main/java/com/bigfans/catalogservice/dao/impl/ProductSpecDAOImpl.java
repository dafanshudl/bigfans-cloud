package com.bigfans.catalogservice.dao.impl;

import com.bigfans.catalogservice.dao.ProductSpecDAO;
import com.bigfans.catalogservice.model.ProductSpec;
import com.bigfans.framework.dao.BeanDecorator;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import org.springframework.stereotype.Repository;

import java.util.List;



/**
 * 
 * @Title: 
 * @Description: 商品和规格值关联表DAO
 * @author lichong 
 * @date 2015年12月21日 下午9:55:11 
 * @version V1.0
 */
@Repository(ProductSpecDAOImpl.BEAN_NAME)
public class ProductSpecDAOImpl extends MybatisDAOImpl<ProductSpec> implements ProductSpecDAO {

	public static final String BEAN_NAME = "productSpecDAO";

	@Override
	public ProductSpec getProductSpec(String pgId, String optionId, String valueId) {
		ParameterMap params = new ParameterMap();
		params.put("pgId", pgId);
		params.put("optionId", optionId);
		params.put("valueId", valueId);
		return getSqlSession().selectOne(className + ".load",params);
	}

	@Override
	public List<ProductSpec> listByProdId(String prodId) {
		ParameterMap params = new ParameterMap();
		params.put("prodId", prodId);
		return getSqlSession().selectList(className + ".list",params);
	}
	
	@Override
	public int batchInsert(List<ProductSpec> objList) {
		return new BeanDecorator(objList).batchInsert();
	}
}

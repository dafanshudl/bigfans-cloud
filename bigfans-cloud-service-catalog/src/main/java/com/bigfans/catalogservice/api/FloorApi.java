package com.bigfans.catalogservice.api;

import com.bigfans.catalogservice.model.Floor;
import com.bigfans.catalogservice.service.category.CategoryService;
import com.bigfans.catalogservice.service.home.FloorService;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author lichong
 * @create 2018-02-24 下午5:44
 **/
@RestController
public class FloorApi {

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private FloorService floorService;

    @GetMapping("/floor")
    public RestResponse listFloor() throws Exception {
        List<Floor> floors = floorService.listFloor();
        return RestResponse.ok(floors);
    }

}

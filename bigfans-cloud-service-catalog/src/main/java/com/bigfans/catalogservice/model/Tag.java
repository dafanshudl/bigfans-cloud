package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.TagEntity;
import lombok.Data;

@Data
public class Tag extends TagEntity {

	private static final long serialVersionUID = -4056447033118407582L;
}

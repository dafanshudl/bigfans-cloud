package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.CategoryEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
public class Category extends CategoryEntity implements Comparable<Category>{
	
	private static final long serialVersionUID = -8722369963771498740L;

	private List<Category> subCats = new ArrayList<Category>();
	private Category parent;
	private List<AttributeOption> attrList = new ArrayList<AttributeOption>();
	
	@Override
	public int compareTo(Category o) {
		int result = 0;
		result = this.level.compareTo(o.getLevel());
		if(result == 0){
			result = this.orderNum.compareTo(o.getOrderNum());
		}
		return result;
	}
	
	public List<Category> toCrumbs(){
		List<Category> crumbs = new ArrayList<Category>();
		this.crumbs(this, crumbs);
		Collections.reverse(crumbs);
		crumbs.add(this);
		return crumbs;
	}
	
	private void crumbs(Category category , List<Category> crumbs){
		if(category.hasParent()){
			crumbs.add(category.getParent());
			this.crumbs(category.getParent(), crumbs);
		}
	}
	
	public boolean hasChild(){
		return subCats != null && !subCats.isEmpty();
	}
	
	public boolean hasParent(){
		return parent != null;
	}

}

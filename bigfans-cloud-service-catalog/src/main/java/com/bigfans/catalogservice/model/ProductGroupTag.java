package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.ProductGroupTagEntity;
import com.bigfans.framework.utils.StringHelper;
import lombok.Data;

@Data
public class ProductGroupTag extends ProductGroupTagEntity {

	private static final long serialVersionUID = -6474776256442538481L;

	private String value;

	public boolean isNew(){
		return StringHelper.isNotEmpty(value) && StringHelper.isEmpty(tagId);
	}

}

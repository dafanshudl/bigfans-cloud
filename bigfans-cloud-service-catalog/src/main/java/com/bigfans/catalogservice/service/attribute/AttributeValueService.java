package com.bigfans.catalogservice.service.attribute;

import com.bigfans.catalogservice.model.AttributeValue;
import com.bigfans.framework.dao.BaseService;

import java.util.List;


/**
 * 
 * @Title: 
 * @Description: 商品属性服务类
 * @author lichong 
 * @date 2015年9月15日 下午11:29:28 
 * @version V1.0
 */
public interface AttributeValueService extends BaseService<AttributeValue> {

	List<AttributeValue> listByOptionId(String attrOptionId) throws Exception;
	
	List<AttributeValue> listById(List<String> idList) throws Exception ;

	List<AttributeValue> listByProduct(String pid) throws Exception;
}

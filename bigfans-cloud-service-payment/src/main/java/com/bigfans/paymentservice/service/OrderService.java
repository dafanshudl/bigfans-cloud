package com.bigfans.paymentservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.paymentservice.model.Order;

/**
 * 
 * @Description: 订单服务类
 * @author lichong 2014年12月5日上午11:24:24
 *
 */
public interface OrderService extends BaseService<Order> {

}

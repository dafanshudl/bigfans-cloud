package com.bigfans.cartservice.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @Description:促销
 * @author lichong 2015年6月6日上午11:29:21
 *
 */
@Data
public class Promotion {

	private static final long serialVersionUID = -6112016318532673405L;

	protected String id;
	protected String name;
	protected boolean selectable;
	protected boolean selected;

}

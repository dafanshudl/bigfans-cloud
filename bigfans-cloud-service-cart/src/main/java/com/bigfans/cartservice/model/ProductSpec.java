package com.bigfans.cartservice.model;

import com.bigfans.cartservice.model.entity.ProductSpecEntity;
import lombok.Data;

@Data
public class ProductSpec extends ProductSpecEntity {

	private static final long serialVersionUID = -6811834593128832769L;

}

package com.bigfans.userservice.listener;

import com.bigfans.framework.kafka.KafkaConsumerBean;
import com.bigfans.model.event.coupon.CouponCreatedEvent;
import com.bigfans.userservice.api.clients.CouponServiceClient;
import com.bigfans.userservice.model.Coupon;
import com.bigfans.userservice.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

@Component
@KafkaConsumerBean
public class CouponListener {

    @Autowired
    private CouponService couponService;
    @Autowired
    private CouponServiceClient couponServiceClient;

    public void on(CouponCreatedEvent event) {
        try {
            String couponId = event.getCouponId();
            CompletableFuture<Coupon> couponFuture = couponServiceClient.getCoupon(couponId);
            Coupon coupon = couponFuture.get();
            couponService.create(coupon);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

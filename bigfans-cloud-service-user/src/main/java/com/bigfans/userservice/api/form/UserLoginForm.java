package com.bigfans.userservice.api.form;

import lombok.Data;

@Data
public class UserLoginForm {

    private String account;
    private String password;
    private boolean rememberMe;

}

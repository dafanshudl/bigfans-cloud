package com.bigfans.orderservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.orderservice.model.Product;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年4月4日下午9:27:43
 *
 */
public interface ProductDAO extends BaseDAO<Product> {

}

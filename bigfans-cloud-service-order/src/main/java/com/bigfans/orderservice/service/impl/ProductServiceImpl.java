package com.bigfans.orderservice.service.impl;

import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.orderservice.dao.ProductDAO;
import com.bigfans.orderservice.model.Product;
import com.bigfans.orderservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @Description:商品服务
 * @author lichong
 * 2015年4月4日下午9:29:26
 *
 */
@Service(ProductServiceImpl.BEAN_NAME)
public class ProductServiceImpl extends BaseServiceImpl<Product> implements ProductService {

	public static final String BEAN_NAME = "productService";

	private ProductDAO productDAO;

	@Autowired
	public ProductServiceImpl(ProductDAO productDAO) {
		super(productDAO);
		this.productDAO = productDAO;
	}

}

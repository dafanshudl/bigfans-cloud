package com.bigfans.orderservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.orderservice.model.DeliveryMethod;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年4月2日上午11:30:10
 *
 */
public interface DeliveryMethodService extends BaseService<DeliveryMethod> {
	
	List<DeliveryMethod> listAllAvailable() throws Exception;
	
}

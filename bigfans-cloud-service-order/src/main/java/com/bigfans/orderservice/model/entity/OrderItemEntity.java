package com.bigfans.orderservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * 
 * @Description:订单条目
 * @author lichong 2014年12月15日下午11:34:27
 *
 */
@Data
@Table(name="OrderItem")
public class OrderItemEntity extends AbstractModel {

	private static final long serialVersionUID = -112379520656314767L;
	// 未评价
	public static final int COMMENT_STATUS_UNCOMMENTED = 0;
	// 已评价
	public static final int COMMENT_STATUS_COMMENTED = 1;

	@Column(name="order_id")
	protected String orderId;
	@Column(name="user_id")
	protected String userId;
	/* 产品id */
	@Column(name="prod_id")
	protected String prodId;
	/* 产品成交价 */
	@Column(name="deal_price")
	protected BigDecimal dealPrice;
	/* 产品成交总价 */
	@Column(name="deal_subtotal")
	protected BigDecimal dealSubTotal;
	/* 购买数量 */
	@Column(name="quantity")
	protected Integer quantity;
	//获得积分
	@Column(name="gained_point")
	protected Float gainedPoint;
	// 评价状态
	@Column(name="comment_status")
	protected Integer commentStatus;
	@Column(name = "promotion")
	protected String promotion;
	
	public String getModule() {
		return "OrderItem";
	}
	
}

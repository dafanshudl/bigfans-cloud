package com.bigfans.orderservice.api.clients;

import com.bigfans.api.clients.ServiceRequest;
import com.bigfans.framework.CurrentUser;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.utils.JsonUtils;
import com.bigfans.orderservice.OrderApplications;
import com.bigfans.orderservice.model.Product;
import com.bigfans.orderservice.model.ProductSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-13 下午7:42
 **/
@Component
public class CatalogServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Product> getProductById(String prodId) {
        return CompletableFuture.supplyAsync(() -> {
            ServiceRequest serviceRequest = new ServiceRequest(restTemplate, OrderApplications.getFunctionalUser());
            Map data = serviceRequest.get(Map.class , "http://catalog-service/product/{prodId}" , prodId);
            List specList = (List)data.get("specList");
            List<ProductSpec> specs = new ArrayList<>();
            for(int i = 0;i<specList.size();i++){
                ProductSpec spec = BeanUtils.mapToModel((Map)specList.get(i) , ProductSpec.class);
                specs.add(spec);
            }
            Product product = BeanUtils.mapToModel((Map) data, Product.class);
            product.setSpecs(JsonUtils.toJsonString(specs));
            return product;
        });
    }

    public CompletableFuture<String> orderStockOut(CurrentUser currentUser , String orderId, Map<String, Integer> prodQuantityMap) {
        return CompletableFuture.supplyAsync(() -> {
            ServiceRequest serviceRequest = new ServiceRequest(restTemplate, currentUser);
            serviceRequest.post("http://catalog-service/orderStockOut?id={id}" , prodQuantityMap ,orderId);
            return null;
        });
    }

    public CompletableFuture<List<ProductSpec>> getSpecsByProductId(String prodId) {
        return CompletableFuture.supplyAsync(() -> {
            ServiceRequest serviceRequest = new ServiceRequest(restTemplate, OrderApplications.getFunctionalUser());
            List list = serviceRequest.get(List.class , "http://catalog-service/specs?prodId={prodId}" , prodId);
            List<ProductSpec> specs = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                ProductSpec productSpec = BeanUtils.mapToModel((Map) list.get(i), ProductSpec.class);
                specs.add(productSpec);
            }
            return specs;
        });
    }

    public CompletableFuture<String> getTagsByProductId(String prodId) {
        return CompletableFuture.supplyAsync(() -> {
            ServiceRequest serviceRequest = new ServiceRequest(restTemplate, OrderApplications.getFunctionalUser());
            List list = serviceRequest.get(List.class , "http://catalog-service/tags?prodId={prodId}" , prodId);
            return null;
        });
    }
}

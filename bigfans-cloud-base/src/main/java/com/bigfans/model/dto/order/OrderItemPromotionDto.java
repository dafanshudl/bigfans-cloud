package com.bigfans.model.dto.order;

import lombok.Data;

/**
 * @author lichong
 * @create 2018-02-16 下午5:57
 **/
@Data
public class OrderItemPromotionDto {

    private String prodId;
    private String promotionId;
    private String promotionName;
    private String promotionMsg;

}

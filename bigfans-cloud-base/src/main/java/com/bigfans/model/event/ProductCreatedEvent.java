package com.bigfans.model.event;

import com.bigfans.framework.event.AbstractEvent;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author lichong
 * @create 2018-02-05 下午8:08
 **/
@Data
public class ProductCreatedEvent extends AbstractEvent{

    public ProductCreatedEvent() {
    }

    public ProductCreatedEvent(String id) {
        this.id = id;
    }

    private String id;
    private String pgId;
    private Date createDate;
    private Date updateDate;
    private Boolean deleted;
    private String name;
    private BigDecimal price;
    private String imagePath;
    private String categoryId;
    private String brandId;
    private String origin;
}

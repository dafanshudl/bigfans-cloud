package com.bigfans.framework.dao;

import java.util.List;
import java.util.Map;

import com.bigfans.framework.cache.CacheEvict;
import com.bigfans.framework.cache.Cacheable;
import com.bigfans.framework.cache.ModelCacheKeyGenerator;
import com.bigfans.framework.model.AbstractModel;
import com.bigfans.framework.model.PageBean;
import com.bigfans.framework.model.PageContext;
import com.bigfans.framework.utils.BeanUtils;


/**
 * @author lichong
 * @version V1.0
 * @Title:
 * @Description:
 * @date 2016年1月16日 下午1:01:14
 */
public abstract class MybatisDAOImpl<M extends AbstractModel> extends BaseDAOImpl implements BaseDAO<M> {

    public static final String INSERT = "insert";
    public static final String UPDATE = "update";
    public static final String DELETE = "delete";
    public static final String LOAD = "load";
    public static final String LIST = "list";
    public static final String PAGE = "page";
    public static final String COUNT = "count";
    public static final String BATCH_INSERT = "batchInsert";

    public static final String START = "start";
    public static final String PAGESIZE = "pagesize";
    public static final String MODEL = "model";
    public static final String PAGEABLE = "pageable";

    public int insert(M e) {
        return new BeanDecorator(e).insert();
    }

    public int batchInsert(List<M> objList) {
        return getSqlSession().insert(className + "." + BATCH_INSERT, objList);
    }

    @CacheEvict(keyGenerator = ModelCacheKeyGenerator.class)
    public int delete(M e) {
        return new BeanDecorator(e).delete();
    }

    @CacheEvict(keyGenerator = ModelCacheKeyGenerator.class)
    public int update(M e) {
        int count = new BeanDecorator(e).update();
        return count;
    }

    @Cacheable(keyGenerator = ModelCacheKeyGenerator.class)
    public M load(M e) {
        return getSqlSession().selectOne(className + "." + LOAD, e);
    }

    @Override
    public List<M> list(M obj, Long start, Long pagesize) {
        Map<String, Object> params = BeanUtils.copyModelToMap(obj);
        return this.list(new ParameterMap(params), start, pagesize);
    }

    @Override
    public List<M> list(ParameterMap params, Long start, Long pagesize) {
        params.put(START, start);
        params.put(PAGESIZE, pagesize);
        params.put(PAGEABLE, false);
        return getSqlSession().selectList(className + "." + LIST, params);
    }

    @Override
    public PageBean<M> page(ParameterMap params, Long start, Long pagesize) {
        params.put(START, start);
        params.put(PAGESIZE, pagesize);
        params.put(PAGEABLE, true);
        List<M> data = getSqlSession().selectList(className + "." + LIST, params);
        return new PageBean<M>(data, PageContext.getDataCount());
    }

    @Override
    public PageBean<M> page(M obj, Long start, Long pagesize) {
        Map<String, Object> params = BeanUtils.copyModelToMap(obj);
        return this.page(new ParameterMap(params), start, pagesize);
    }

    public Long count(M e) {
        return getSqlSession().selectOne(className + "." + COUNT, e);
    }

}

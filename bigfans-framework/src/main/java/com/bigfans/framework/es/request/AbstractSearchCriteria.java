package com.bigfans.framework.es.request;

import java.util.List;

import org.elasticsearch.search.sort.FieldSortBuilder;

public class AbstractSearchCriteria {

	protected String index;
	protected String type;
	protected int from;
	protected int size;
	protected float minScore;
	protected List<FieldSortBuilder> sortList;
	// 设置自定义排序时候同样进行打分计算
	protected boolean trackScores;
	
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public float getMinScore() {
		return minScore;
	}

	public void setMinScore(float minScore) {
		this.minScore = minScore;
	}

	public List<FieldSortBuilder> getSortList() {
		return sortList;
	}

	public void setSortList(List<FieldSortBuilder> sortList) {
		this.sortList = sortList;
	}

	public boolean isTrackScores() {
		return trackScores;
	}

	public void setTrackScores(boolean trackScores) {
		this.trackScores = trackScores;
	}

}

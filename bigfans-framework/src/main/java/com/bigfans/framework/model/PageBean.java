package com.bigfans.framework.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @Description: 分页
 * @author lichong
 * 2014年11月25日上午10:53:55
 *
 */
public class PageBean<T> implements Serializable{

	private static final long serialVersionUID = -8848616175295644951L;
	private static Long DEFAULT_PAGE_SIZE = 20L;
	private static String PAGE_SEPERATOR = "....";

	private Long pageSize = DEFAULT_PAGE_SIZE; // 每页的记录数

	private List<T> data ; // 当前页中存放的记录

	private Long totalCount; // 总记录数
	
	private Long currentPage; // 当前页
	
	private Long maxShowPage = 6l; // 最多显示多少页,默认为6页,那么显示效果为1,2,3,4,5,6,'.....',10

	/**
	 * 默认构造方法.
	 * 
	 * @param start
	 *            本页数据在数据库中的起始位置
	 * @param totalSize
	 *            数据库中总记录条数
	 * @param pageSize
	 *            本页容量
	 * @param data
	 *            本页包含的数据
	 */
	public PageBean(List<T> data) {
		this(data , PageContext.getDataCount());
	}
	
	public PageBean(List<T> data , Long dataCount) {
		this(data , dataCount ,PageContext.getCurrentPage() ,PageContext.getPageSize());
	}
	
	public PageBean(List<T> data , Long totalCount , Long currentPage , Long pagesize) {
		setParam(currentPage ,pagesize , data , totalCount);
	}

	public void setParam(Long currentPage, Long pageSize, List<T> data , Long totalCount) {
		this.pageSize = pageSize;
		this.currentPage = currentPage;
		this.totalCount = totalCount;
		this.data = data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}
	
	public List<T> getData() {
		return data;
	}
	
	/**
	 * 取总记录数.
	 */
	public Long getTotalCount() {
		return this.totalCount;
	}

	/**
	 * 取总页数.
	 */
	public Long getTotalPageCount() {
		if(totalCount == null || pageSize == null){
			return 0l;
		}
		if (totalCount % pageSize == 0)
			return totalCount / pageSize;
		else
			return totalCount / pageSize + 1;
	}

	/**
	 * 取每页数据容量.
	 */
	public Long getPageSize() {
		return pageSize;
	}

	/**
	 * 取当前页中的记录.
	 */
	public List<T> getResult() {
		return data;
	}

	public Long getCurrentPage() {
		return currentPage;
	}

	public boolean isHasPreviousPage() {
		return currentPage > 1;
	}

	public boolean isHasNextPage() {
		return currentPage < this.getTotalPageCount();
	}

	/**
	 * 获取任一页第一条数据在数据集的位置，每页条数使用默认值.
	 * 
	 * @see #getStartOfPage(int,int)
	 */
	protected static Long getStartOfPage(Long pageNo) {
		return getStartOfPage(pageNo, DEFAULT_PAGE_SIZE);
	}

	/**
	 * 获取任一页第一条数据在数据集的位置.
	 * 
	 * @param pageNo
	 *            从1开始的页号
	 * @param pageSize
	 *            每页记录条数
	 * @return 该页第一条数据
	 */
	public static Long getStartOfPage(Long pageNo, Long pageSize) {
		return (pageNo - 1) * pageSize;
	}

	/**
	 * 需要显示的页, 比如:如果最多显示6页,一共总记录数有10页,当前是第一页,那么showdPage久应该是1,2,3,4,5,6,'.....',10
	 */
	public List<String> getShowedPage() {
		List<String> showedPages = new ArrayList<String>();
		Long lastPage = getTotalPageCount();
		
		if(currentPage <= maxShowPage){
			for (long i = 1; i <= lastPage; i++) {
				showedPages.add(String.valueOf(i));
			}
			if(lastPage > maxShowPage){
				showedPages.add(PAGE_SEPERATOR);
				showedPages.add(String.valueOf(lastPage));
			}
			return showedPages;
		}
		
		if(currentPage >= (lastPage - maxShowPage)){
			for (long i = lastPage; i > maxShowPage; i--) {
				showedPages.add(String.valueOf(i));
			}
			Collections.reverse(showedPages);
			showedPages.add(PAGE_SEPERATOR);
			showedPages.add("1");
			return showedPages;
		}
		
		if((currentPage > maxShowPage) &&(currentPage < (lastPage - maxShowPage))){
			Long middleNo = Long.valueOf(maxShowPage/2);
			showedPages.add("1");
			showedPages.add(PAGE_SEPERATOR);
			for (long i = currentPage-middleNo + 1; i < currentPage+middleNo; i++) {
				showedPages.add(String.valueOf(i));
			}
			showedPages.add(PAGE_SEPERATOR);
			showedPages.add(String.valueOf(lastPage));
			return showedPages;
		}
		
		return showedPages;
	}

	public Long getMaxShowPage() {
		return maxShowPage;
	}

	public void setMaxShowPage(Long maxShowPage) {
		this.maxShowPage = maxShowPage;
	}

}
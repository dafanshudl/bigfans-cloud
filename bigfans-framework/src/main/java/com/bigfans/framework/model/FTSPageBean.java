package com.bigfans.framework.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bigfans.framework.es.request.AggregationResult;


/**
 * 
 * @Title: 
 * @Description: 全文检索的pagebean封装
 * @author lichong 
 * @date 2015年10月8日 上午9:15:23 
 * @version V1.0
 */
public class FTSPageBean<T> extends PageBean<T> {

	private static final long serialVersionUID = 5896722957255029805L;

	public FTSPageBean(List<T> data, Long totalCount) {
		super(data, totalCount);
	}
	
	private List<AggregationResult> aggregationList ;

	public List<AggregationResult> getAggregationList() {
		return aggregationList;
	}
	
	public Map<String , List<AggregationResult>> getAggregationMap(){
		Map<String , List<AggregationResult>> resultMap = new HashMap<String , List<AggregationResult>>();
		if(aggregationList == null){
			return resultMap;
		}
		for (AggregationResult aggregationResult : aggregationList) {
			String name = aggregationResult.getName();
			if(resultMap.containsKey(name)){
				List<AggregationResult> list = resultMap.get(name);
				if(!list.contains(aggregationResult)){
					resultMap.get(name).add(aggregationResult);
				}
			}else{
				List<AggregationResult> list = new ArrayList<AggregationResult>();
				list.add(aggregationResult);
				resultMap.put(name, list);
			}
		}
		return resultMap;
	}

	public void setAggregationList(List<AggregationResult> aggregationList) {
		this.aggregationList = aggregationList;
	}

}
